'''
@file       main.py

@brief      Runs on Nucleo startup, initiates bluetooth module task.

@details    Sets bluetooth module task to have an interval of 0.01 seconds or 
            run at a frequency of 100 Hz. Upon startup of the Nucleo the bluetooth
            task begins running and waiting for input from the BLE module.

@image      html Lab_5_Task_Diagram.PNG width=1000px
@image      html Lab_5_Transition_Diagram.PNG width=1000px
'''

from Lab_5_FSM import BluetoothTask

Task = BluetoothTask(10000)

while True:
    Task.run()
