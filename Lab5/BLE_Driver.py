'''
@file       BLE_Driver.py

@brief      This file defines a bluetooth module driver.

@details    This file contains a class which can be called from another
            file to initialize the bluetooth module, write through the module, 
            and read through the module. It also allows for control of an LED.

'''

import pyb
from pyb import UART

class BLEDriver:
    '''
    @brief      This class creates a driver for the BLE bluetooth module.
    
    @details    This class allows the user to initialize the bluetooth module
                along with an led. The user can then read and write through
                the bluetooth module and turn on and off the LED.
    '''
    
    def __init__ (self , uart , baudrate , led_pin):
        '''
        @param uart     Allows for input of uart to be used by module.
        @param baudrate Allows for input of desired baudrate for uart com.
        @param led_pin  Allows for variable led pin.
        '''
        self.myuart = UART(uart , baudrate)
        
        self.led_pin = pyb.Pin(pyb.Pin.cpu.led_pin)
        
    def read (self):
        
        if self.myuart.any() != 0:
            val = int(self.myuart.readline())
            return(val)
        else:
            pass
        
    def write (self , write_data):
        '''
        @param write_data   Allows for input to be sent through module.
        '''        
        self.myuart.write(write_data)
        
    def check (self):
        
        return(self.myuart.any())
        
    def led (self , on_off):
        '''
        @param on_off   Determines if led is turned on or off.
        '''
        if(on_off == 'ON'):
            self.led_pin.high()
        elif(on_off == 'OFF'):
            self.led_pin.low()
        else:
            pass
