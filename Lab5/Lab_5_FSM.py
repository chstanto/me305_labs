'''
@file       Lab_5_FSM.py

@brief      This file uses the BLE driver to adjust LED blinking frequency.

@details    Using the BLE driver, this file gets user input via bluetooth
            which is used to adjust the blinking frequency of a desired LED.
            The accuracy of the blinking frequency is determined by the
            interval used because the LED can only be turned on and off
            once per interval. The range of blinking frequencies which this
            file supports is from 0 hz to 10 hz. See BLE_Driver class for more
            information on the bluetooth module and how it is used.


'''

import utime
from pyb import UART
import BLE_Driver.py

myuart = UART(3)

class BluetoothTask:
    ## Constant defining state 0 - Initialization
    S0_INIT                     = 0
    
    ## Constant defining state 1 - Wait For Input
    S1_WAIT_FOR_INPUT           = 1

    def __init__(self,interval):
        '''
        @brief     Creates an EncoderTask object 
        @param     interval     Input for time between runs in microseconds
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Variable representing LED on or off (1 on and 0 off)
        self.led = 0
        
        ## Variable counting time passed between each LED switch
        self.delta = 0
        
    def run(self):
        '''
        @brief      Runs one iteration of the BluetoothTask
        '''
        ## Updates current time
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                
                BLE_Driver.__init__(self , 3 , 9600 , 'A5')
                
                self.transitionTo(self.S1_WAIT_FOR_INPUT)
            
            elif(self.state == self.S1_WAIT_FOR_INPUT):
                
                if(BLE_Driver.read == True):
                    self.led_freq = BLE_Driver.read
                    self.delta = 0
                    
                if(0 < self.led_freq <= 10):
                    
                    if(self.delta < 1/(2*self.led_freq)):
                        self.delta += self.interval
                        
                    elif(self.delta >= 1/(2*self.led_freq)):
                        
                        if(self.led == 0):
                            BLE_Driver.led(self , 'ON')
                            self.delta = 0
                            
                        elif(self.led == 1):
                            BLE_Driver.led(self , 'OFF')
                            self.delta = 0
                            
                elif(self.led_freq == 'X'):
                    BLE_Driver.led(self , 'OFF')
                    self.delta = 0
                    
                elif(self.led_freq == 'O'):
                    BLE_Driver.led(self , 'ON')
                    self.delta = 0
                
                else:
                    pass
                
        else:
                # Invalid state code (error handling)
                pass
            
            
        # Specifying the next time the task will run
        self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    def transitionTo(self, newState):
        '''
        @brief      Sets new state
        @param      newState    Variable representing the desired new state
        '''
        self.state = newState