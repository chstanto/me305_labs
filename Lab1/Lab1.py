'''
@file       Lab1.py
@brief      This file contains a fibonacci function.
@details    The function defined in this file as fib() accepts an input from
            the user indicating a fibonacci index and returns the fibonacci
            number at the specified index.
@author     Cole Stanton
@copyright This file is licensed under the creative commons ...

'''

def fib (idx):
    
    '''
    @brief      This constructor creates the fibonacci function.
    @details    The fibonacci index user input is run through a series of
                if/elif/else loops to find the output. The first if statement
                checks if the input is 0 and returns the appropriate output,
                the following elif statement checks if the input is 1 and 
                returns the appropriate output. The second elif statement
                contains a while loop which iterates through the fibonacci
                sequence, adding each value to the fib_seq matrix. This is
                accomplished using the variable n which is increased by 1 for
                each loop until it reaches the index input. The final else
                statement is included to account for negative number inputs.
                Please see following link for source code:
                https://bitbucket.org/chstanto/me305_labs/src/master/Lab1/Lab1.py
    
    '''
    
    print ('Calculating Fibonacci number at '
           'index n = {:}.'.format(idx))
    
    ## Integer indicating the input at which the function will begin iterating
    n = 2
    
    ## List describing the first two values in the fibonacci sequence
    fib_seq = [0,1]
    
    if idx == 0:
        print (0)
    elif idx == 1:
        print (1)
    elif idx > 1:
        while n <= idx:
            x = fib_seq[n-2] + fib_seq[n-1]
            fib_seq.append(x)
            n = n+1
        print (x)
    else:
        print ('Error: Please enter a positive integer')
   
#if __name__ == '__main__':
#    fib(10)


