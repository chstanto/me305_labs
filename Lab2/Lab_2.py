'''
@file       Lab_2.py

@brief      This file defines two finite state machines to blink LEDs.

@details    This file defines two finite state machines, the first of which
            blinks a virtual LED and the second of which modulates the
            brightness of LED A5 on the Nucleo microcontroller. The LED 
            brightness modulation is in the form of a sawtooth wave.
'''

import pyb
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

class VirtualLedBlink:
    '''
    @brief      A finite state machine to control a virtual LED
    @details    This class implements a finite state machine to alternate
                a virtual LED between on and off states.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT              = 0
    
    ## Constant defining State 1 - Pulsing Virtual LED
    S1_LED_PULSE         = 1
    
    
    def __init__(self, Interval):
        '''
        @brief      Creates a VirtualLedBlink object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = pyb.millis()
        
        ## The interval of time, in milliseconds, between runs of the task
        self.interval = Interval
        
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
        
    def TransitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        ## Updates the current time after each run through the task
        self.curr_time = pyb.millis()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                print('LED OFF')
                self.LED_STATE = 0
                self.TransitionTo(self.S1_LED_PULSE)
            
            elif(self.state == self.S1_LED_PULSE):
                
                if(self.LED_STATE == 0):
                    print('LED ON')
                    self.LED_STATE = 1
                    
                elif(self.LED_STATE == 1):
                    print('LED OFF')
                    self.LED_STATE = 0
            
            self.next_time += self.interval # updating the "Scheduled" timestamp

    
class PhysicalLedBlink:
    '''
    @brief      A finite state machine to control a physical Nucleo LED.
    @details    This class implements a finite state machine to control the
                brightness of an LED based on a sawtooth wave.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT              = 0
    
    ## Constant defining State 1 - Sawtooth Brightness Incrementing
    S1_LED_INCREMENT     = 1
    
    def __init__(self, Interval):
        '''
        @brief      Creates a PhysicalLedBlink object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = pyb.millis()
        
        ## The interval of time, in milliseconds, between runs of the task
        self.interval = Interval
        
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
        
        ## Sets the percentage of LED brightness
        self.LED_BRIGHTNESS = 0
        
    def TransitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        ## Updates the current time after each run through the task
        self.curr_time = pyb.millis()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                self.TransitionTo(self.S1_LED_INCREMENT)
                t2ch1.pulse_width_percent(self.LED_BRIGHTNESS)
            
            elif(self.state == self.S1_LED_INCREMENT):
                
                if(self.LED_BRIGHTNESS < 100):
                    self.LED_BRIGHTNESS += 10
                    t2ch1.pulse_width_percent(self.LED_BRIGHTNESS)
                    
                elif(self.LED_BRIGHTNESS == 100):
                    self.LED_BRIGHTNESS = 0
                    t2ch1.pulse_width_percent(self.LED_BRIGHTNESS)
            
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    