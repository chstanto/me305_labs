'''
@file       Lab_2_Main.py

@brief      This file defines and runs one virtual and one physical LED task.

@details    This file defines the interval required to run two LED tasks. The
            first task represents a virtual LED which will print "LED ON" and
            "LED OFF" repeatedly. The second task controls a physical LED 
            built into the NUCLEO Microcontroller. The task modulates the
            brightness of the LED A5 in the form of a sawtooth wave. Both 
            tasks run simultaneously, showing off the microcontrollers ability
            to multitask. See image below for transition diagrams describing
            each task.
            
@image      html Lab_2_Transition_Diagram.PNG
'''

from Lab_2 import VirtualLedBlink, PhysicalLedBlink

import pyb
# pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
# tim2 = pyb.Timer(2, freq = 20000)
# t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

## Define max run time as current time + 10s
run_time = pyb.millis() + 10000

## Virtual LED task
TaskA = VirtualLedBlink(500)

## Physical LED task
TaskB = PhysicalLedBlink(500)

while(pyb.millis() < run_time):
    TaskA.run()
    TaskB.run()

