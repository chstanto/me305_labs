'''
@file       Lab_3_EncoderTask.py

@brief      This file defines an FSM to update and read the encoder driver.

@details    Data from the encoder driver is generated using the update
            function, and upon each update the current position and delta 
            values stored in the shares file are also updated to be retrieved
            by the user interface task.
@image      html Lab_3_EncoderTask_Diagram.PNG width=1200px
'''

import Lab_3_Shares as shares
import utime
from Lab_3_Encoder_Driver import EncoderDriver

class EncoderTask:
    ## Constant defining state 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining state 1 - Update
    S1_UPDATE           = 1

    def __init__(self,interval):
        '''
        @brief     Creates an EncoderTask object 
        @param     interval     Input for time between runs in microseconds
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief      Runs one iteration of the EncoderTask
        '''
        ##
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_UPDATE)
                shares.enc_zero = 0
                EncoderDriver.__init__(self , 3 , 'A6' , 'A7' , 1 , 2)
            
            elif(self.state == self.S1_UPDATE):
                EncoderDriver.update(self)
                shares.enc_pos = EncoderDriver.get_position(self)
                shares.enc_delta = EncoderDriver.get_delta(self)
                if shares.enc_zero == 1:
                    EncoderDriver.set_position(self)
                    shares.enc_zero = 0
                else:
                    pass
                
            else:
                # Invalid state code (error handling)
                pass
            
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    def transitionTo(self, newState):
        '''
        @brief      Sets new state
        @param      newState    Variable representing the desired new state
        '''
        self.state = newState
        
        
        
        
        