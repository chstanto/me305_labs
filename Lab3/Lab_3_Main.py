'''
@file       Lab_3_Main.py

@brief      This file is the main file for Lab 3.

@details    Defines both EncoderTask and UITask and runs them both for 
            one minute. The interval for both tasks is defined as one second.
'''

import utime
from  Lab_3_UITask import UITask
from Lab_3_EncoderTask import EncoderTask

start_time = utime.ticks_us()
run_time = utime.ticks_add(start_time , 60000000)

TaskA = EncoderTask(1000000)

TaskB = UITask(1000000)

while(utime.ticks_us() <= run_time):
    TaskA.run()
    TaskB.run()