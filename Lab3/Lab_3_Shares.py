'''
@file       Lab_3_Shares.py

@brief      This file contains shared variables for EncoderTask and UITask

@details    Variables in this file are left blank so that while both 
            EncoderTask and UITask are running simultaneously, the values
            can be updated by EncoderTask and read by UITask.
'''

## Defines shared position varibale to be updated and read by two files
enc_pos     = None

## Defines shared delta variable to be updated and read by two files
enc_delta   = None

## Defines shared variable to zero the encoder position
enc_zero    = None