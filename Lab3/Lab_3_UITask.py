'''
@file       Lab_3_UITask.py

@brief      This file defines a class which handles user interface.

@details    This file looks for three inputs from a user and responds with
            data corresponding to each input. This inputs relate to the motor
            position and data is retrieved from the shares file.
@image      html Lab_3_UITask_Diagram.PNG width=1200px

'''

import Lab_3_Shares as shares
import utime
from pyb import UART

class UITask:

    ## Constant defining state 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining state 1 - Interface
    S1_INTERFACE            = 1

    def __init__(self,interval):
        '''
        @brief     Creates a UITask object 
        @param     interval     Input for time between runs in microseconds
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Serial port
        self.ser = UART(2)
        
    def run(self):
        '''
        @brief      Runs one iteration of the UITask
        '''
        ##
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_INTERFACE)
                print('Type p for motor position')
                print('Type d for motor displacement since last update')
                print('Type z to zero motor position')
            elif(self.state == self.S1_INTERFACE):
                
                if self.ser.any():
                    self.char = self.ser.readchar()
                    
                    if self.char == 122:
                        shares.enc_zero = 1
                        
                    elif self.char == 112:
                        print('Current Motor Position: ' + str(shares.enc_pos) + 'Degrees')
                        #print('p')
                        
                    elif self.char == 100:
                        print('Most Recent Motor Delta: ' + str(shares.enc_delta) + 'Degrees')
                        #print('d')
                    
                    else:
                        print('Invalid Input Character')
                    
                else:
                    #print('No Input')
                    pass
            else:
                # Invalid state code (error handling)
                pass
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    def transitionTo(self, newState):
        '''
        @brief      Sets new state
        @param      newState    Variable representing the desired new state
        '''
        self.state = newState
        

        
        