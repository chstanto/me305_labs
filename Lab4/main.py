'''
@file       main.py

@brief      Runs on Nucleo startup, initiates encoder task.

@details    Sets encoder task to have an interval of 0.2 seconds or run at
            a frequency of 5 Hz. Upon startup of the Nucleo the encoder
            task begins running and waiting for serial communication from
            python.

@image      html Lab_4_Encoder_Diagram.PNG width=1000px
'''

from Lab_4_Encoder_FSM import EncoderTask

Task = EncoderTask(200000)

while True:
    Task.run()

    # if myuart.any() != 0:
    #     val = myuart.readchar()
    #     myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo')
        
