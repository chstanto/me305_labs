'''


'''

import serial
ser = serial.Serial(port = 'COM3' , baudrate = 115273 , timeout = 1)

def sendChar():
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    return myval

for n in range(1):
    print(sendChar())

ser.close()

# import matplotlib.pyplot as plt
# import numpy as np

# time = np.array([0,1,2,3,4,5,6,7,8,9,10])
# vals = time**2

# plt.plot(time,vals,'ro-')
# plt.xlabel('Time [s]')
# plt.ylabel('Angle [deg]')
# plt.grid(True)
# plt.show

# np.savetxt('File_Name.csv' , vals , delimiter = ',')