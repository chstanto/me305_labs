'''
@title      Lab_4_UI.py

@brief      Sets up FSM for user interface.

@details    This script includes a finite state machine which requests input
            from the user. After recieving input from the user, the script
            sends code to the Nucleo microcontroller via serial communication.
            The sent code either starts data collection from the motor
            encoder or stops data collection. This script then recieves an
            array of encoder data from the Nucleo and plots the data as a
            function of time. The combined time and encoder data is then saved
            as a csv file.

@image      html Lab_4_UI_Diagram.PNG width=1000px

'''

import time
import array
import serial
import matplotlib.pyplot as plt
import numpy as np

ser = serial.Serial(port = 'COM3' , baudrate = 115273 , timeout = 1)

class UITask:
    ## Constant defining state 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining state 1 - Update
    S1_UPDATE           = 1

    def __init__(self,interval):
        '''
        @brief     Creates an EncoderTask object 
        @param     interval     Input for time between runs in microseconds
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## Defines the user input variable
        self.input = 0
        
        ## Defines time array to be updated upon every loop
        self.time_array = []
        
        ## Variable tracking runs during data collection
        self.time_var = 0
        
        ## Variable controlling if statements recieving from Nucleo
        self.send_recieve = 0
        
    def run(self):
        '''
        @brief      Runs one iteration of the EncoderTask
        '''
        ## Updates current time
        self.curr_time = time.time()
        
        if(self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_UPDATE)
                
                
            elif(self.state == self.S1_UPDATE):
                
                inv = input('Spyder: Input G to begin data collection \n')
                self.input = str(inv).encode('ascii')
                
                if(self.input == 0):
                    print('Input G to begin data collection, S to stop')
                
                elif(self.input == 71):
                    if(self.send_recieve == 0):
                        ser.write(self.input)
                        self.time_array.append(0)
                        self.send_recieve = 1
                        self.time_var += 1
                    elif(self.send_recieve == 1):
                        if(self.time_var <= 49):
                            self.time_array.append(self.interval/1000000*self.time_var)
                            self.time_var += 1
                        else:
                            pos_data = ser.readline()
                            time_array = array.array(self.time_array)
                            self.send_recieve = 3
                            
                elif(self.input == 83):
                    if(self.send_recieve == 1):
                        ser.write(self.input)
                        self.send_recieve += 1
                    elif(self.send_recieve == 2):
                        pos_data = ser.readline()
                        time_array = array.array(self.time_array)
                        self.send_recieve = 3
                        
                elif(self.input == 3):
                    plt.plot(time_array , pos_data)
                    plt.xlim([0,10])
                    plt.xlabel('Time, t [sec]')
                    plt.ylabel('Encoder Position [deg]')
                    plt.title('Encoder Position vs Time')
                    plt.show()
                    
                    np.savetxt('PositionTimeData.csv' , (time_array , pos_data) , delimiter = ',')
                    
                    self.send_recieve = 0
                    self.input = 0
                    self.time_array = []
                    self.time_var = 0
                    
                    
                else:
                    print('Invalid Character Input')
                    self.input = 0
                
        else:
            # Invalid state code (error handling)
            pass
            
            
        # Specifying the next time the task will run
        self.next_time = self.next_time + self.interval
        
    def transitionTo(self, newState):
        '''
        @brief      Sets new state
        @param      newState    Variable representing the desired new state
        '''
        self.state = newState
        
ser.close()
        