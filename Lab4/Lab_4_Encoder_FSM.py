'''
@title      Lab_4_Encoder_FSM.py

@brief      This script sets up a FSM to update and record the encoder position.

@details    In this script a finite state machine is set up to update an
            array of encoder position data at any given interval. Upon recieving
            an input from the serial port, data collection from the encoder
            is started. Data is recorded for 10 seconds unless an additional
            command is recieved to stop data collection. The script then sends
            the collected data via the serial port.

@image      html Lab_4_Encoder_Diagram.PNG width=1000px

'''

import utime
from Lab_3_Encoder_Driver import EncoderDriver
from pyb import UART
import array

## Defines serial input port
myuart = UART(2)

class EncoderTask:
    ## Constant defining state 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining state 1 - Update
    S1_UPDATE           = 1

    def __init__(self,interval):
        '''
        @brief     Creates an EncoderTask object 
        @param     interval     Input for time between runs in microseconds
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Sets initial array value
        self.list_val = 0
        
        ## Sets initial serial value
        self.val = 0
        
        ## Defines list to be updated
        self.pos_list = []
        
    def run(self):
        '''
        @brief      Runs one iteration of the EncoderTask
        '''
        ## Updates current time
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.transitionTo(self.S1_UPDATE)
                EncoderDriver.__init__(self , 3 , 'A6' , 'A7' , 1 , 2)
            
            elif(self.state == self.S1_UPDATE):
                EncoderDriver.update(self)
                if(myuart.any() != 0):
                    self.val = myuart.readchar()
                if(self.val == 71):
                    if(self.list_val < 50):
                        self.pos_list.append(EncoderDriver.get_position(self))
                        self.list_val += 1
                    else:
                        data_array = array.array(self.pos_list)
                        myuart.write('i' , (data_array))
                        self.list_cal = 0
                        self.val = 0
                        self.pos_list = []
                        
                elif(self.val == 83):
                    data_array = array.array(self.pos_list)
                    myuart.write('i' , (data_array))
                    self.list_val = 0
                    self.val = 0
                    self.pos_list = []
        else:
                # Invalid state code (error handling)
                pass
            
            
        # Specifying the next time the task will run
        self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    def transitionTo(self, newState):
        '''
        @brief      Sets new state
        @param      newState    Variable representing the desired new state
        '''
        self.state = newState