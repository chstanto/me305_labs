'''
@file       Lab_4_UI_Main.py

@brief      Runs on Nucleo startup, initiates encoder task.

@details    Sets encoder task to have an interval of 0.2 seconds or run at
            a frequency of 5 Hz. Upon startup of the Nucleo the encoder
            task begins running and waiting for serial communication from
            python.
            
@image      html Lab_4_UI_Diagram.PNG width=1000px

'''

import Lab_4_UI

Task = Lab_4_UI.UITask(0.2)

while True:
    Task.run()
