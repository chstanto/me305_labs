'''
@file Lecture_HW1

This file is an adaptation of the windshield wiper example presented in class
intended to represent an elevator working between two floors.

Their is a button on either floor.

There is also a limit switch at either end of the elevators path.
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control an Elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator between two floors.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT              = 0
    
    ## Constant defining State 1
    S1_STOPPED_AT_FLOOR1 = 1
    
    ## Constant defining State 2
    S2_STOPPED_AT_FLOOR2 = 2
    
    ## Constant defining State 3
    S3_MOVING_DOWN       = 3
    
    ## Constant defining State 4
    S4_MOVING_UP         = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING        = 5
    
    def __init__(self, interval, Motor, Button_1, Button_2, First, Second):
        '''
        @brief      Creates a TaskElevator object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.Motor = Motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A button used to call the elevator to floor 1
        self.Button_1 = Button_1
        
        ## A button used to call the elevator to floor 2
        self.Button_2 = Button_2
        
        ## The sensor object used for the first floor limit
        self.First = First
        
        ## The sensor object used for the second floor limit
        self.Second = Second
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                
                # Run State 0 Code
                self.transitionTo(self.S3_MOVING_DOWN)
                self.Motor = 2
            
            elif(self.state == self.S1_STOPPED_AT_FLOOR1):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                
                # Run State 1 Code                
                if self.Button_2.getButtonState() == 1:
                    self.transitionTo(self.S4_MOVING_UP)
                    self.Motor = 1
            
            elif(self.state == self.S2_STOPPED_AT_FLOOR2):
                print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
               
                # Run State 2 Code                
                if self.Button_1.getButtonState() == 1:
                    self.transitionTo(self.S3_MOVING_DOWN)
                    self.Motor = 2
            
            elif(self.state == self.S3_MOVING_DOWN):
                print(str(self.runs) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                if self.First.getButtonState():
                    self.transitionTo(self.S1_STOPPED_AT_FLOOR1)
                    self.Motor = 0
            
            elif(self.state == self.S4_MOVING_UP):
                print(str(self.runs) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                if self.Second.getButtonState():
                    self.transitionTo(self.S2_STOPPED_AT_FLOOR2)
                    self.Motor = 0
                    
            elif(self.state == self.S5_DO_NOTHING):
                print(str(self.runs) + ' State 5 {:0.2f}'.format(self.curr_time - self.start_time))
                
            
            else:
                # Uh-oh state (undefined state)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary elevator user to call the elevator. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized 1 or 0 value.
        @return     An integer representing the state of the button.
        '''
        return choice([0, 1])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to move the elevator
                between the first and second floors.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Forward(self):
        '''
        @brief Moves the motor forward
        '''
        if self.motor == 1:
            print('Motor moving elevator up')
    
    def Reverse(self):
        '''
        @brief Moves the motor forward
        '''
        if self.motor == 2:
            print('Motor moving elevator down')
    
    def Stop(self):
        '''
        @brief Moves the motor forward
        '''
        if self.motor == 0:
            print('Motor/elevator Stopped')

