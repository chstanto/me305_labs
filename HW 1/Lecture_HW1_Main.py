'''
@file       Lecture_HW1_Main.py

@author     Cole Stanton

@brief      This file defines motors, buttons, and limit switches required
            for two elevators to run simultaneously. Also defnined in this
            script are input parameters for the TaskElevator function along
            with the number of loops that should be run. Reference 
            Lecture_HW1.py for the task source code. 

@image      html Elevator_Transition_Diagram.png
'''

from Lecture_HW1 import Button, MotorDriver, TaskElevator

# Button and Motor definitions for elevator A task (1/2)
        
## Motor A Object
MotorA = MotorDriver()

## Button Object for "First Floor Button A"
Button_1A = Button('PA6')

## Button Object for "Second Floor Button A"
Button_2A = Button('PA7')

## Button Object for "First Floor Limit Switch A"
FirstA = Button('PA8')

## Button Object for "Second Floor Limit Switch A"
SecondA = Button('PA9')

## Task object A
taskA = TaskElevator(0.1, MotorA, Button_1A, Button_2A, FirstA, SecondA) # Will also run constructor

# Button and Motor definitions for elevator B task (2/2)

## Motor B Object
MotorB = MotorDriver()

## Button Object for "First Floor Button B"
Button_1B = Button('PB6')

## Button Object for "Second Floor Button B"
Button_2B = Button('PB7')

## Button Object for "First Floor Limit Switch B"
FirstB = Button('PB8')

## Button Object for "Second Floor Limit Switch B"
SecondB = Button('PB9')

## Task object B
taskB = TaskElevator(0.1, MotorB, Button_1B, Button_2B, FirstB, SecondB) # Will also run constructor

# To run the task call task1.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    taskA.run()
    taskB.run()
