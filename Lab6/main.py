'''
@title      main.py

@brief      Sets up Closed Loop task and runs on Nucleo start up.

@details    This file defines the Closed Loop FSM task running at an interval
            of 10000 microseconds or 0.01 seconds. This file has been named
            main so that it will run on the Nucleo startup. For more
            information on this project see Lab_6_ClosedLoop_FSM.py.
            
@image      html Lab_6_Task_Diagram.PNG width=1000px
@image      html Lab_6_ClosedLoop_Transition_Diagram.PNG width=1000px

'''

import Lab_6_ClosedLoop_FSM

Task = Lab_6_ClosedLoop_FSM.UITask(10000)

while True:
    Task.run()