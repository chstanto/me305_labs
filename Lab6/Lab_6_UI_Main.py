'''
@file       Lab_6_UI_Main.py

@brief      Sets up user interface task when called in spyder console.

@details    This file is the main file on the front end of Lab 6. When this
            file is executed in the Spyder console window it will run the 
            lab 6 user interface task. The task has been set up to run at an
            interval of 0.01 seconds. For more information please see
            Lab_6_UI.py .
            
@image      html Lab_6_Task_Diagram.PNG width=1000px

'''

import Lab_6_UI

Task = Lab_6_UI.UITask(0.01)

while True:
    Task.run()
