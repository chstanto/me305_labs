'''
@file       Lab_6_UI.py

@brief      Sets up FSM for user interface.

@details    This script includes a finite state machine which requests input
            from the user. Once the desired proportional gain has been recieved
            from the user, the script sends code to the Nucleo microcontroller 
            via serial communication. After sending the Kp value, the program
            waits for a response which should contain an array of output
            velocity values. These values are then plotted as a function of
            time and overlayed with reference velocity data. For more
            information on the backend of this project see main.py .

@image      html Lab_6_UI_Transition_Diagram.PNG width=1000px

'''

import time
import array
import serial
import matplotlib.pyplot as plt

ser = serial.Serial(port = 'COM3' , baudrate = 115273 , timeout = 1)

class UITask:
    ## Constant defining state 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining state 1 - Send Kp
    S1_SEND_KP           = 1
    
    ## Constant defining state 2 - Receive
    S2_RECEIVE          = 2

    def __init__(self , interval):
        '''
        @brief     Creates an EncoderTask object 
        @param     interval     Input for time between runs in microseconds
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## Defines time array to be updated upon every loop
        self.time_array = []
        
        ## Variable tracking runs during data collection
        self.time_var = 0
        
    def run(self):
        '''
        @brief      Runs one iteration of the UITask
        '''
        ## Updates current time
        self.curr_time = time.time()
        
        if(self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                
                self.kp = input('Spyder: Input desired proportional gain value: \n')
                self.transitionTo(self.S1_UPDATE)
                
                
            elif(self.state == self.S1_UPDATE):
                
                if(self.kp != None):
                    if(self.kp == int):
                        if(self.kp > 0):
                            ser.write(self.kp)
                            self.transitionTo(self.S2_RECEIVE)
                        else:
                            print('Invalid Proportional Gain Input')
                            self.transitionTo(self.S0_INIT)
                    else:
                        print('Invalid Proportional Gain Input')
                        self.transitionTo(self.S0_INIT)
                else:
                    pass
                
            elif(self.state == self.S2_RECEIVE):
                
                self.time_array.append(self.interval*self.time_var)
                self.time_var += 1
                if(ser.readline() != None):
                    v_actual_data = ser.readline()
                    v_ref_data = array.array('I' , 100*[40])
                    
                    overlay = plt.subplots()
                    overlay.plot(self.time_array , v_actual_data)
                    overlay.plot(self.time_array , v_ref_data)
                    overlay.xlim([0,10])
                    overlay.xlabel('Time, t [sec]')
                    overlay.ylabel('Output Shaft Velocity [rad/s]')
                    overlay.title('Output Angular Velocity vs Time')
                    overlay.show()
                    
                    self.time_array = []
                    self.time_var = 0
                    
                    self.transitionTo(self.S0_INIT)
        else:
            # Invalid state code (error handling)
            pass
            
            
        # Specifying the next time the task will run
        self.next_time = self.next_time + self.interval
        
    def transitionTo(self, newState):
        '''
        @brief      Sets new state
        @param      newState    Variable representing the desired new state
        '''
        self.state = newState

ser.close()
        