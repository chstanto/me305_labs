'''
@file       Lab_7_Shares.py

@brief      Sets up shared varibles for lab 7.

@details    This file allows for reference data to be shared between the main
            file and the fsm. The main file is responsible for reading all of
            the data in the provided reference csv. While reading data the
            time, position, and velocity lists in this file are updated.

'''

## Defines shared reference time variable
ref_time        = []

## Defines shared reference position variable
ref_position    = []

## Defines shared reference velocity variable
ref_velocity    = []