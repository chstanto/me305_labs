'''
@file       Encoder_Driver.py

@brief      This file defines a class capable of reading from the encoder.

@details    The encoder driver set up in this file is able to output
            to another file the motor position, the most recent motor delta,
            and it is also able to zero the motor position.
'''

import pyb

class EncoderDriver:
    '''
    @brief      This class defines the three functions of the encoder driver.
    
    @details    The three functions defined by this class are the
                initialization function, the get position function, the set
                position function, and the zero function. All three are 
                intended to be called by another file.
    '''
    
    def __init__(self, enc_num , timer_number, ch1_pin, ch2_pin):
        '''
        @brief      Defines parameters needed for an encoder object
        @details    This function accepts 5 inputs to define the appropriate
                    pins and channels needed to use any encoder. Also defines
                    variables needed when updating encoder outputs.
        @param      enc_num         Indicates which encoder to initialize
        @param      timer_number    Variable for desired timer number
        @param      ch1_pin         Variable for desired chanel 1 pin
        @param      ch2_pin         Variable for desired chanel 2 pin
        @param      ch1_num         Variable for desired chanel 1 number
        @param      ch2_num         Variable for desired chanel 2 number
        '''
        
        if(enc_num == 1):
            self.tim1 = pyb.Timer(timer_number)
            self.tim1.init(prescaler = 0 , period = 0xFFFF)
        
            self.tim1.channel(1 , pin = ch1_pin , mode = pyb.Timer.ENC_AB)
            self.tim1.channel(2 , pin = ch2_pin , mode = pyb.Timer.ENC_AB)
        
            self.delt1       = [0,0]
            self.zero1       = 0
            self.pos1        = 0
            self.position1   = 0
            
        elif(enc_num == 2):
            self.tim2 = pyb.Timer(timer_number)
            self.tim2.init(prescaler = 0 , period = 0xFFFF)
        
            self.tim2.channel(1 , pin = ch1_pin , mode = pyb.Timer.ENC_AB)
            self.tim2.channel(2 , pin = ch2_pin , mode = pyb.Timer.ENC_AB)
        
            self.delt2       = [0,0]
            self.zero2       = 0
            self.pos2        = 0
            self.position2   = 0
    
    def update(self , enc_num):
        '''
        @brief      Function which consistently updates motor position.
        @details    Creates an array of two variables which upon every update
                    is changed to reflect the most recent position and the
                    last position. This array makes it easy to access the
                    most recent position as well as the delta between the
                    last update.
        @param enc_num Indicates which encoder should be updated.
        '''
        if(enc_num == 1):
            self.pos1            = self.tim1.counter() - self.zero1
            self.delt1[0]        = self.delt1[1]
        
            if (self.pos1 - self.delt1[0]) >= 32768:
                self.delt1[1]    = self.pos1 - 65535
            else:
                self.delt1[1]    = self.pos1
        
            self.delta1 = self.delt1[1]/4*360/7 - self.delt1[0]/4*360/7
            
        elif(enc_num == 2):
            self.pos2            = self.tim2.counter() - self.zero2
            self.delt2[0]        = self.delt2[1]
        
            if (self.pos2 - self.delt2[0]) >= 32768:
                self.delt2[1]    = self.pos2 - 65535
            else:
                self.delt2[1]    = self.pos2
        
            self.delta2 = self.delt2[1]/4*360/7 - self.delt2[0]/4*360/7
        
    def get_position(self , enc_num):
        '''
        @brief      This function returns the current position of the motor.
        @details    This function converts the position value into units of
                    degrees and then returns the most recent position.
        @param enc_num Indicates which encoder to get position from.
        '''
        if(enc_num == 1):
            self.position  = self.delt1[1]/4*360/7
            return self.position
        
        if(enc_num == 2):
            self.position  = self.delt2[1]/4*360/7
            return self.position
        
    def set_position(self , enc_num):
        '''
        @brief      This function sets the position of the motor to zero.
        @details    Sets the zero value to the current position of the motor
                    so that during the next update the position will reflect
                    the zero. 
        @param enc_num Indicates which encoder to set position for.
        '''
        if(enc_num == 1):
            self.zero1       = self.tim1.counter()
            self.delt1[1]    = 0
            
        if(enc_num == 2):
            self.zero2       = self.tim2.counter()
            self.delt2[1]    = 0
        
    def get_delta(self , enc_num):
        '''
        @brief      This function returns the most recent delta.
        @details    Returns the difference between the motor position at the
                    last two position updates.
        @param enc_num Indicates which encoder to pull the delta of.
        '''
        
        if(enc_num == 1):
            return self.delta1
        
        if(enc_num == 2):
            return self.delta2
        
        