'''
@file       Lab_7_UI_FSM.py

@brief      Sets up FSM for user interface.

@details    This script includes a finite state machine which requests input
            from the user. Once the user has inputted a 1 to begin reference
            tracking, the script sends reference data to the Nucleo microcontroller 
            via serial communication. After sending the data, the program
            waits for a response which should contain an array of output
            velocity and position values. These values are then plotted as a function of
            time. For more information on the backend of this project see Lab_7_Main.py.

@image      html Lab_7_UI_Transition_Diagram.PNG width=1000px

'''

import time
import array
import serial
import matplotlib.pyplot as plt
import Lab_7_Shares as shares

ser = serial.Serial(port = 'COM3' , baudrate = 115273 , timeout = 1)

class UITask:
    ## Constant defining state 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining state 1 - Send Reference
    S1_SEND_REF         = 1
    
    ## Constant defining state 2 - Receive
    S2_RECEIVE          = 2
    
    ## Constant defining state 3 - Return
    S3_RETURN           = 3

    def __init__(self , interval):
        '''
        @brief     Creates an EncoderTask object 
        @param     interval     Input for time between runs in microseconds
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## Defines time array to be updated upon every loop
        self.ref_time = array.array('I' , [])
        
        ## Defines reference position array to be updated
        self.ref_pos = array.array('I' , [])
        
        ## Defines reference velocity array to be updated
        self.ref_vel = array.array('I' , [])
        
        ## Variable for reference data interval
        self.ref_int = shares.ref_time[1]
        
        ## Empty array for velocity and position data
        self.data = array.array('I' , [])
        
    def run(self):
        '''
        @brief      Runs one iteration of the UITask
        '''
        ## Updates current time
        self.curr_time = time.time()
        
        if(self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                
                self.runs = 0
                self.input = input('Spyder: Input 1 to begin reference tracking: \n')
                self.transitionTo(self.S1_SEND_REF)
                
            elif(self.state == self.S1_SEND_REF):
                
                if(self.input != None):
                    if(self.input == 1):
                        if(self.runs <= (len(shares.ref_velocity)-1)*self.ref_int/self.interval):
                            ser.write(shares.ref_velocity[self.runs*(self.interval/self.ref_int)])
                            
                            self.ref_time.append(shares.ref_time[self.runs*(self.interval/self.ref_int)])
                            self.ref_pos.append(shares.ref_position[self.runs*(self.interval/self.ref_int)])
                            self.ref_vel.append(shares.ref_velocity[self.runs*(self.interval/self.ref_int)])
                            
                            self.runs += 1
                        else:
                            self.transitionTo(self.S2_RECIEVE)
                            self.runs +=1
                    else:
                        print('Invalid Input')
                        self.transitionTo(self.S0_INIT)
                else:
                    pass
                
            elif(self.state == self.S2_RECEIVE):
                
                if(ser.readline() != None):
                    self.data.append(ser.readline())
                elif(len(self.data) > 0):
                    self.transitionTo(self.S3_RETURN)
                else:
                    pass
                
            elif(self.state == self.S3_RETURN):
                
                data_length = len(self.data)/2
                p_actual = self.data[0:data_length]
                v_actual = self.data[(data_length + 1):data_length*2]
                
                overlay, axs = plt.subplots(2 , 1)
                
                axs[0].plot(self.ref_time , v_actual)
                axs[0].set_xlim(0 , 15)
                axs[0].set_ylim(-60 , 60)
                axs[0].set_xlabel('Time [min]')
                axs[0].set_ylabel('Velocity [rpm]')
                
                axs[1].plot(self.ref_time , p_actual)
                axs[1].set_xlim(0 , 15)
                axs[1].set_ylim(0 , 800)
                axs[1].set_xlabel('Time [min]')
                axs[1].set_ylabel('Velocity [rpm]')
                
                overlay.show()
                
                n = 0
                J = 0
                
                while(n <= data_length):
                    J += (self.ref_vel[n] - v_actual[n])**2 + (self.ref_pos[n] - p_actual[n])**2
                
                print('J = ' , J/data_length)
                
                self.ref_time = array.array('I' , [])
                self.ref_pos = array.array('I' , [])
                self.ref_vel = array.array('I' , [])
                self.ref_int = shares.ref_time[1]
                self.data = array.array('I' , [])
                
                shares.ref_time = []
                shares.ref_position = []
                shares.ref_velocity = []
                
                self.transitionTo(self.S0_INIT)
                
        else:
            # Invalid state code (error handling)
            pass
            
            
        # Specifying the next time the task will run
        self.next_time = self.next_time + self.interval
        
    def transitionTo(self, newState):
        '''
        @brief      Sets new state
        @param      newState    Variable representing the desired new state
        '''
        self.state = newState

ser.close()
        