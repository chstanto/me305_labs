'''
@file       Lab_7_ClosedLoop_FSM.py

@brief      Finite state machine allowing for proportional control of motor.

@details    This file defines a finite state machine which uses three 
            additional classes to control a motor. The three classes used
            are a Motor Driver, an Encoder Driver, and a Proportional Controller
            class. This FSM has four states: initialization, wait for input,
            motor control, and respond. Upon initialization the three classes
            are properly set up, after which the program waits for input from
            Spyder via serial communication. After recieving an input with the
            motor reference data, the program enables the motor and 
            attempts to track the provided reference velocity data. Velocity and position 
            data is saved through each loop and sent back to Spyder after reference 
            data runs out. The program then resets vars and waits for another input.
            For more information on the classes used in this file please see
            Encoder_Driver.py , Motor_Driver.py , and Lab_7_ClosedLoop.py .

@image      html Lab_7_ClosedLoop_Transition_Diagram.PNG width=1000px

'''

import utime
from Encoder_Driver import EncoderDriver
from Motor_Driver import MotorDriver
from Lab_6_ClosedLoop import ClosedLoop
from pyb import UART
import array

## Defines serial input port
myuart = UART(2)

class EncoderTask:
    ## Constant defining state 0 - Initialization
    S0_INIT                     = 0
    
    ## Constant defining state 1 - Wait for Input
    S1_WAIT_FOR_INPUT           = 1
    
    ## Constant defining state 2 - Update Data
    S2_MOTOR_CONTROL            = 2
    
    ## Constant defining state 3 - Respond
    S3_RESPOND                  = 3

    def __init__(self , interval):
        '''
        @brief     Creates an EncoderTask object 
        @param     interval     Input for time between runs in microseconds
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Defines empty array to be filled with motor velocity data.
        self.v_array = array.array('I' , [])
        
        ## Defines empty array to be filled with motor position data.
        self.p_array = array.array('I' , [])
        
        ## Defines empty array to be filled by reference velocity data
        self.ref_v = array.array('I' , [])
        
        ## Variable tracking the number of runs
        self.runs = 0
        
        ## Variable defining the controller proportional gain
        self.kp = 0.1
        
    def run(self):
        '''
        @brief      Runs one iteration of the EncoderTask
        '''
        ## Updates current time
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                EncoderDriver.__init__(self , 1 , 4 , 'B6' , 'B7')
                MotorDriver.__init__(self , 1 , 3 , 'A15' , 'B4' , 'B5')
                ClosedLoop.__init__(self)
                MotorDriver.enable(self)
                ClosedLoop.set_kp(self , self.kp)
                self.transitionTo(self.S1_WAIT_FOR_INPUT)
            
            elif(self.state == self.S1_WAIT_FOR_INPUT):
                if(myuart.any() != 0):
                    self.ref_v.append(myuart.readchar())
                elif(len(self.ref_v) > 0):
                    self.transitionTo(self.S2_MOTOR_CONTROL)
                else:
                    pass
            elif(self.state == self.S2_MOTOR_CONTROL):
                
                EncoderDriver.update(self , 1)
                
                if(self.runs <= len(self.ref_v)):
                    self.data_time += self.interval
                    # Finds current velocity in rad/s
                    v_actual = EncoderDriver.get_delta(self , 1)/(self.interval*57.3/1000000)
                    # Uses controller to find next duty value
                    duty = ClosedLoop.run(self , v_actual)
                    #sets motor duty cycle based on controller output
                    MotorDriver.set_duty(self , duty , 1) 
                    self.v_array.append(v_actual)
                    self.p_array.append(EncoderDriver.get_position(self , 1))
                    
                else:
                    MotorDriver.set_duty(self , 0 , 1)
                    MotorDriver.disable(self)
                    self.transitionTo(self.S3_RESPOND)
            
            elif(self.state == self.S3_RESPOND):
                
                data_array = array.array('I' , [self.p_array , self.v_array])
                myuart.write('I' , (data_array))
                
                self.v_array = []
                self.p_array = []
                self.transitionTo(self.S1_WAIT_FOR_INPUT)

        else:
                # Invalid state code (error handling)
                pass
            
            
        # Specifying the next time the task will run
        self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    def transitionTo(self, newState):
        '''
        @brief      Sets new state
        @param      newState    Variable representing the desired new state
        '''
        self.state = newState