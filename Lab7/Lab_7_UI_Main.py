'''
@file       Lab_7_UI_Main.py

@brief      Sets up user interface task when called in spyder console.

@details    This file is the main file on the front end of Lab 7. When this
            file is executed in the Spyder console window it will run the 
            lab 7 user interface task. The task has been set up to run at an
            interval of 0.01 seconds. This file also interprets a csv file
            with reference data. For more information please see
            Lab_7_UI_FSM.py. 
            
@image      html Lab_7_Task_Diagram.PNG width=1000px

'''

import Lab_6_UI
import Lab_7_Shares as shares

ref = open('reference.csv')

while True:
    line = ref.readline()
    if(line == ''):
        break
    else:
        (t,v,x) = line.strip().split(',');
        shares.time.append(float(t))
        shares.velocity.append(float(v))
        shares.position.append(float(x))

ref.close()
Task = Lab_6_UI.UITask(0.01)

while True:
    Task.run()
