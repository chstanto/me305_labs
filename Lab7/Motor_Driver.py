'''
@file       Motor_Driver.py

@brief      Sets up a class which can adjust power to the motor.

@details    This file defines a class which will allow a user to adjust the
            duty cycle of any two motors connected to the Nucleo. This file makes
            use of the pyb module for setting up pins and timers.
'''

import pyb

class MotorDriver:
    '''
    @brief      Sets up methods required to control a motor.
    
    @details    The Motor Driver class allows the user to set up two motors
                using the __init__ method. This method assigns the appropriate
                pins, timers, and channels required by the motors. The class also
                includes the enable and disable methods which set the motors
                enable pin either to high or low. Finally the set duty method 
                allows the user to input a desired motor duty cycle after it has
                been enabled.
    '''
    
    def __init__ (self , motor_num , timer , nSLEEP_pin , IN1_pin , IN2_pin):
        '''
        @brief  Sets up pins and timers for two motors (1 and 2).
        @parram nSLEEP_pin  A pin object used to define enable pin.
        @param motor_num    A variable defining which of two motors should
                            be adjusted.
        @param IN1_pin      A pin object used to define half bridge input 1.
        @param IN2_pin      A pin object used to define half bridge input 2.
        @param timer        A timer object used PWM generation on inputs 1,2.
        
        '''
        self.enable_pin = pyb.Pin(pyb.Pin.cpu.nSLEEP_pin , pyb.Pin.OUT_PP)
        self.enable_pin.low()
        
        if (motor_num == 1):
            self.M1_pos = pyb.Pin(pyb.Pin.cpu.IN1_pin)
            self.M1_neg = pyb.Pin(pyb.Pin.cpu.IN2_pin)
            
            self.timer1 = pyb.Timer(timer , freq = 20000)
            self.M1CH1 = self.timer1.channel(1 , pyb.Timer.PWM , pin = self.M1_pos)
            self.M1CH2 = self.timer1.channel(2 , pyb.Timer.PWM , pin = self.M1_neg)
            
            print('M1 Driver Created')
            
        elif (motor_num == 2):
            self.M2_pos = pyb.Pin(pyb.Pin.cpu.IN1_pin)
            self.M2_neg = pyb.Pin(pyb.Pin.cpu.IN2_pin)
            
            self.timer2 = pyb.Timer(timer , freq = 20000)
            self.M2CH1 = self.timer2.channel(1 , pyb.Timer.PWM , pin = self.M1_pos)
            self.M2CH2 = self.timer2.channel(2 , pyb.Timer.PWM , pin = self.M1_neg)
        
            print('M2 Driver Created')
        
    def enable (self):
        '''
        @brief  Sets the motors enable pin to high, allowing operation.
        '''
        self.enable_pin.high()
        
        print ('Enabling Motors')
        
    def disable (self):
        '''
        @brief  Sets the motors enable pin to low, disabling operation.
        '''
        self.enable_pin.low()
            
        print('Disabling Motors')
        
    def set_duty (self , duty , motor_num):
        
        '''
        @brief  Adjusts the motors duty cycle based on input parameter.
        @param duty         A signed integer holding PWM signal duty cycle.
        @param motor_num    A variable defining which of two motors should
                            be adjusted.
        '''
        if (motor_num == 1):
            self.M1CH1.pulse_width_percent(0)
            self.M1CH2.pulse_width_percent(duty)
            
            print('M1 Duty Set')
            
        elif (motor_num == 2):
            self.M2CH1.pulse_width_percent(0)
            self.M2CH2.pulse_width_percent(duty)

            print('M2 Duty Set')
        
    
    