'''
@file       Lab_7_ClosedLoop.py

@brief      Defines classes used to determine proportional gain for controller.

@details    This file sets up the class and methods required to proportionally
            control a motor. 

'''

class ClosedLoop:
    '''
    @brief      This class defines the three functions of the encoder driver.
    
    @details    The three functions defined by this class are the
                initialization function, the get position function, the set
                position function, and the zero function. All three are 
                intended to be called by another file.
    '''
    
    def __init__(self):
        '''
        @brief      Method initializes the motor controller.
        @param ref_v    Defines the reference velocity.
        @param kp       Defines user inputted proportional gain.
        '''
        self.kp = 0
        
    def run(self , actual_v , ref_v):
        '''
        @brief      Computes and returns actuation value.
        '''
        return(self.kp*(ref_v - actual_v))
        
    def get_kp(self):
        '''
        @brief      Returns the current value of the proportional gain.
        '''
        return(self.kp)
        
    def set_kp(self , kp):
        '''
        @brief      Sets the proportional gain value for the controller.
        '''
        self.kp = kp
        